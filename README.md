# Message project installation

This project can be runned locally using docker-compose.

## Prerequisites

To run manually, you need to have docker installed on your machine.

## Message producer

Clone the project:

```bash
git clone https://gitlab.com/messagingproject/message-producer.git
```

Create the docker image:

```bash
cd message-producer
docker build -t messages-producer:1.0.0 .
```

## Message consumer

Clone the project:

```bash
git clone https://gitlab.com/messagingproject/messages-consumer.git
```

Create the docker image:

```bash
cd messages-consumer
docker build -t messages-consumer:1.0.0 .
```

## Client

Clone the project:

```bash
git clone https://gitlab.com/messagingproject/client.git
```

Create the docker image:

```bash
cd client
docker build -t messages-client:1.0.0 .
```

## Running docker compose

Clone this project:

```bash
git clone https://gitlab.com/messagingproject/project-compose.git
```

Run docker compose:

```bash
cd project-compose
docker-compose up
```

Now the app should be running.

## View the app

Go to [localhost:3000](http://localhost:3000).
